package minieduca.atp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetATPGreetingController {
    @GetMapping("/atp-greeting")
    public String index() {
        return "Hola desde ATP :)";
    }
}